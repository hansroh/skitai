set -xe
pytest level1/test_parameter_class.py --disable-pytest-warnings -x
pytest level1/test_params.py --disable-pytest-warnings -x
pytest level2/test_datamodel.py --disable-pytest-warnings -x
pytest level3/test_argspec.py --disable-pytest-warnings -x
pytest level3/test_atila_cli.py --disable-pytest-warnings -x
pytest level3/test_atila_websocket.py --disable-pytest-warnings -x
pytest level3/test_parameters.py --disable-pytest-warnings -x
pytest level3/test_app_validate.py --disable-pytest-warnings -x
