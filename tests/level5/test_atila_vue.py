import os
from rs4.webkit import jwt
import time
from atila_vue.auth import handler

SECRET_KEY = "DstAin8IMr2v1IbWguirp3UOQ6g8qa2TmsYfEm2VVJdRgkg=="
def generate_token (email, grp = None):
    h = handler.ExampleAuthHandler (300, 800)
    user = h.get_account (email)
    if not user:
        user = h.create_account (email)
    payload = h.get_jwt_payload (user)
    if grp:
        payload ['grp'] = grp
    payload ['iat'] = int (time.time ())
    payload ['exp'] = int (payload ['iat'] + 300)
    payload ['typ'] = 'at'
    return jwt.gen_token (SECRET_KEY.encode (), payload)



def test_atila_example_auth ():
    from atila_vue.auth import handler

    h = handler.ExampleAuthHandler (100, 400)
    assert h.get_account ('hans') == None

    user = h.create_account ('hans')
    assert user.email == 'hans'
    assert user.grp == ['user']
    assert user.status == 'ACTIVE'
    assert not user.email_verified

    payload = h.get_jwt_payload (user)
    assert set (payload.keys ()) == {"uid", "grp", "email"}

    user2 = h.create_account ('hans2')
    assert user2.email == 'hans2'

    user = h.get_account ('hans')
    assert user.email == 'hans'

    h.set_email_verified (user)
    assert user.email_verified

    h.delete_account (user)
    user = h.get_account ('hans')
    assert user.status == 'DELETED'


def test_atila_vue (launch):
    script = '../../atila-vue/example/skitaid.py'
    if not os.path.exists (script):
        return

    with launch (script) as engine:
        # static test
        resp = engine.get ('/atila-vue/components/dev/vuex-state.vue')
        assert resp.status_code == 200

        resp = engine.get ('/robots.txt')
        assert "Disallow: /" in resp.text

        resp = engine.get ('/sw-config.js')
        assert resp.status_code == 404

        resp = engine.get ('/firebase-config.js')
        assert "vapidKey" in resp.text

        # API test
        resp = engine.get ('/ping')
        assert resp.text == 'pong'

        resp = engine.get ('/base-template')
        assert "Template Version:" in resp.text

        # View test
        resp = engine.get ('/mpa')
        assert "Multi Page App" in resp.text
        assert "/sw/serviceworker-register.js" in resp.text
        assert "loadModule" in resp.text
        assert "router" not in resp.text

        resp = engine.get ('/spa')
        assert "VueRouter.createWebHistory ('/spa')" in resp.text
        assert "items/:id" in resp.text
        assert "/sw/serviceworker-register.js" in resp.text
        assert "loadModule" in resp.text
        assert "router" in resp.text


def test_atila_vue_auth_api (launch):
    script = '../../atila-vue/example/skitaid.py'
    if not os.path.exists (script):
        return

    with launch (script) as engine:
        # auth API test
        axios = engine.axios

        resp = axios.post ("/api-auth/create_user_with_email_and_password", {"email": "hansroh@example.com", "password": "1111"})
        assert resp.status_code == 400

        resp = axios.post ("/api-auth/create_user_with_email_and_password", {"email": "hansroh@example", "password": "xxx1111;"})
        assert resp.status_code == 400

        resp = axios.post ("/api-auth/create_user_with_email_and_password", {"email": "hansroh@example.com", "password": "xxx1111;"})
        data = resp.json ()
        assert data ['access_token']
        assert data ['refresh_token']

        resp = axios.post ("/api-auth/create_user_with_email_and_password", {"email": "hansroh@example.com", "password": "xxx1111;"})
        assert resp.status_code == 400

        resp = axios.post ("/api-auth/signin_with_email_and_password", {"email": "hansroh@example.com", "password": "xxx1111;;"})
        assert resp.status_code == 401

        resp = axios.post ("/api-auth/signin_with_email_and_password", {"email": "hansroh@example.com", "password": "xxx1111;"})
        data = resp.json ()
        assert data ['access_token']
        assert data ['refresh_token']

        resp = axios.post ("/api-auth/access_token", {})
        assert resp.status_code == 400

        resp = axios.post ("/api-auth/access_token", {'refresh_token': 'asdasd'})
        assert resp.status_code == 401

        resp = axios.post ("/api-auth/access_token", {'refresh_token': data ['access_token']})
        assert resp.status_code == 401

        resp = axios.post ("/api-auth/access_token", {'refresh_token': data ['refresh_token']},
            headers = {"Authorization": "Bearer {}".format (data ['access_token'])}
        )
        assert resp.status_code == 200

        resp = axios.get ("/api-auth/onetime_token?timeout=3",
            headers = {"Authorization": "Bearer {}".format (data ['access_token'])}
        )
        assert resp.status_code == 200
        ott = resp.data ['token']

        resp = axios.get (f"/api-auth/onetime_token?ott={ott}",
            headers = {"Authorization": "Bearer {}".format (data ['access_token'])}
        )
        assert resp.status_code == 200
        assert resp.data ['verified']
        time.sleep (5)

        resp = axios.get (f"/api-auth/onetime_token?ott={ott}",
            headers = {"Authorization": "Bearer {}".format (data ['access_token'])}
        )
        assert resp.status_code == 401

        resp = axios.post ("/api-auth/access_token", {'refresh_token': data ['refresh_token']},
            headers = {"Authorization": "Bearer {}".format (data ['refresh_token'])}
        )
        assert resp.status_code == 400

        resp = axios.post ("/api-auth/access_token", {'refresh_token': data ['access_token']},
            headers = {"Authorization": "Bearer {}".format (data ['access_token'])}
        )
        assert resp.status_code == 400

        req = '../../atila-vue/example/dep/.protected'
        if not os.path.exists (req):
            return

        ID_TOEKN = {'iss': 'https://securetoken.google.com/atila-vue', 'aud': 'atila-vue', 'auth_time': 1688909507, 'user_id': 'HLVNXkjGg6RZQBKza5JxjScHGUo2', 'sub': 'HLVNXkjGg6RZQBKza5JxjScHGUo2', 'iat': 1688909507, 'exp': 1688913107, 'email': 'UNITTEST@email.com', 'email_verified': False, 'firebase': {'identities': {'email': ['hansroh@example.com']}, 'sign_in_provider': 'password'}, 'uid': 'HLVNXkjGg6RZQBKza5JxjScHGUo2'}
        resp = axios.post ("/api-auth/firebase/signin_with_id_token", {'id_token': 'dummy', 'decoded_token': ID_TOEKN})
        data = resp.json ()
        assert data ['access_token']
        assert data ['refresh_token']

        # resp = axios.post ("/api-auth/firebase/signin_with_id_token", {'id_token': 'x'})
        # assert resp.status_code == 400


def test_permission (launch):
    script = '../../atila-vue/example/skitaid.py'
    if not os.path.exists (script):
        return

    user_token = generate_token ('user')
    staff_token = generate_token ('staff', ['staff'])
    admin_token = generate_token ('admin', ['admin'])
    claims = jwt.decode (user_token)

    with launch (script) as engine:
        resp = engine.axios.get ('/owner/me')
        assert resp.status_code == 401

        resp = engine.axios.get ('/owner/me', headers = {"Authorization": f"Bearer {user_token}"})
        assert resp.status_code == 200

        resp = engine.axios.get ('/owner/me', headers = {"Authorization": f"Bearer {staff_token}"})
        assert resp.status_code == 200

        resp = engine.axios.get ('/owner/100', headers = {"Authorization": f"Bearer {staff_token}"})
        assert resp.status_code == 403

        resp = engine.axios.get ('/owner/100', headers = {"Authorization": f"Bearer {admin_token}"})
        assert resp.status_code == 200


        resp = engine.axios.get ('/owner_staff/me')
        assert resp.status_code == 401

        resp = engine.axios.get ('/owner_staff/me', headers = {"Authorization": f"Bearer {user_token}"})
        assert resp.status_code == 200

        resp = engine.axios.get ('/owner_staff/{uid}'.format (**claims), headers = {"Authorization": f"Bearer {staff_token}"})
        assert resp.status_code == 200

        resp = engine.axios.get ('/owner_staff/100', headers = {"Authorization": f"Bearer {user_token}"})
        assert resp.status_code == 403

        resp = engine.axios.get ('/owner_staff/{uid}'.format (**claims), headers = {"Authorization": f"Bearer {user_token}"})
        assert resp.status_code == 200

        resp = engine.axios.get ('/owner_staff/{uid}'.format (**claims))
        assert resp.status_code == 401


        resp = engine.axios.get ('/anyuser/me')
        assert resp.status_code == 401

        resp = engine.axios.get ('/anyuser/me', headers = {"Authorization": f"Bearer {user_token}"})
        assert resp.status_code == 200

        resp = engine.axios.get ('/anyuser/100', headers = {"Authorization": f"Bearer {user_token}"})
        assert resp.status_code == 200


        resp = engine.axios.get ('/staff/me')
        assert resp.status_code == 401

        resp = engine.axios.get ('/staff/me', headers = {"Authorization": f"Bearer {user_token}"})
        assert resp.status_code == 403

        resp = engine.axios.get ('/staff/100', headers = {"Authorization": f"Bearer {user_token}"})
        assert resp.status_code == 403

        resp = engine.axios.get ('/staff/100', headers = {"Authorization": f"Bearer {staff_token}"})
        assert resp.status_code == 200


        resp = engine.axios.get ('/admin/100')
        assert resp.status_code == 401

        resp = engine.axios.get ('/admin/100', headers = {"Authorization": f"Bearer {user_token}"})
        assert resp.status_code == 403

        resp = engine.axios.get ('/admin/100', headers = {"Authorization": f"Bearer {staff_token}"})
        assert resp.status_code == 403

        resp = engine.axios.get ('/admin/100', headers = {"Authorization": f"Bearer {admin_token}"})
        assert resp.status_code == 200
