import skitai
import confutil
import pprint
import re
import pytest


def test_parameters (app):
    @app.route ("/3")
    def index3 (context, a = 1):
        return "Hello"

    @app.route ("/4")
    def index4 (context, a = 1):
        raise TypeError

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("/4?a=1")
        print (resp.text)
        assert resp.status_code == 502

        resp = cli.get ("/3?b=1")
        print (resp.text)
        assert resp.status_code == 400


