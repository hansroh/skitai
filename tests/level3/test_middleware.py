import confutil
import asyncio
import time

def test_middleware (fscope_app):
    app = fscope_app
    @app.route ("/1")
    def index (context):
        return context.API ()

    @app.route ("/2")
    def index (context):
        assert 1 == 0

    @app.middleware ('perf')
    def add_process_time (context, app, call_next):
        result = call_next ()
        context.response.set_header ("X-Process-Time", 100)
        return result

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("/1")
        assert resp.status_code == 200
        assert resp.get_header ('x-process-time') == '100'

        resp = cli.get ("/2")
        assert resp.status_code == 502
        assert resp.get_header ('x-process-time') is None


    def add_tail (context, app, call_next):
        result = call_next ()
        result ['tail'] = True
        return result
    app.add_middleware ('tail', add_tail)

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("/1")
        assert resp.status_code == 200
        assert resp.get_header ('x-process-time') == '100'
        assert resp.data ['tail']

        resp = cli.get ("/2")
        assert resp.status_code == 502
        assert resp.get_header ('x-process-time') is None



