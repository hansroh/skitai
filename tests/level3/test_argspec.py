from __future__ import annotations
import skitai
import confutil
import pprint
import re
import os
import pytest
from atila import datamodel

def test_spec1 (app):
    @app.route ("/")
    @app.inspect ("URL", ["limit"])
    def index (context, limit):
        return ""

    @app.route ("/2")
    @app.inspect ("FORM", ["limit"])
    def index2 (context, limit):
        return ""

    @app.route ("/3")
    @app.inspect ("JSON", ["limit"])
    def index3 (context, limit):
        return ""

    @app.route ("/4")
    @app.inspect ("ARGS", ["limit"])
    def index4 (context, limit):
        return ""

    @app.route ("/5")
    @app.inspect ("ARGS", emails = ["email"], uuids = ["uuid"])
    def index5 (context, email = None, uuid = None):
        return ""

    @app.route ("/6")
    @app.inspect ("ARGS", a__gte = 5, b__between = (-4, -1), c__in = (1, 2))
    def index6 (context, **url):
        return ""

    @app.route ("/7")
    @app.inspect ("ARGS", a = re.compile ("^hans"), b__len__between = (4, 8))
    def index7 (context, a = None, b = None):
        return ""

    @app.route ("/7-1")
    @app.inspect ("ARGS", a__len = 4)
    def index7_1 (context, a = None):
        return ""

    @app.route ("/8")
    @app.inspect ("DATA", ["limit"])
    def index8 (context, limit):
        return ""

    @app.route ("/9")
    @app.inspect ("DATA", lists = ['a'])
    def index9 (context, a):
        return ""

    @app.route ("/10")
    @app.inspect ("DATA", bools = ['a'])
    def index10 (context, a):
        return ""

    @app.route ("/11")
    @app.inspect ("DATA", dicts = ['a'])
    def index11 (context, a):
        return ""

    @app.route ("/12")
    @app.inspect ("DATA", strings = ['a'])
    def index12 (context, a):
        return ""

    @app.route ("/13")
    @app.argspec (a = str, b = int)
    def index13 (context, a, b = None):
        return ""

    @app.route ("/14")
    @app.argspec (a__startswith = 'a_', b__notstartwith = 'a_', c__endswith = '_z', d__notendwith = '_z', e__contains = '_x' , f__notcontain = '_x')
    def index14 (context, a, b, c, d, e, f):
        return ""

    @app.route ("/15")
    @app.argspec (d___k__1__gte = 10)
    def index15 (context, d):
        return ""

    @app.route ("/16")
    @app.argspec (d___k__1__len__gte = 3)
    def index16 (context, d):
        return ""

    @app.route ("/16-1")
    @app.argspec (d___k__1__len = 3)
    def index16_1 (context, d):
        return ""

    @app.route ("/17")
    @app.argspec (d___k__len__gte = 3)
    def index17 (context, d):
        return ""

    def verify (context, d):
        if d == True:
            return 777
        raise context.HttpError ("444 Bad Request")

    @app.route ("/18")
    @app.argspec (d = verify)
    def index18 (context, d):
        return context.API (r = d)

    @app.route ("/19")
    @app.argspec (d = int)
    def index19 (context, d):
        return context.API (r = d)

    with app.test_client ("/", confutil.getroot ()) as cli:
        app.expose_spec = True
        resp = cli.get ("/")
        assert resp.status_code == 400

        resp = cli.get ("/?limit=4")
        assert resp.status_code == 200

        resp = cli.get ("/2?limit=4")
        assert resp.status_code == 200

        resp = cli.post ("/2", {"limit": 4})
        assert resp.status_code == 200

        resp = cli.post ("/2", {})
        assert resp.status_code == 400

        api = cli.api ()
        resp = api ("2").post ({"limit": 4})
        assert resp.status_code == 400

        api = cli.api ()
        resp = api ("3").post ({"limit": 4})
        assert resp.status_code == 200

        api = cli.api ()
        resp = api ("4").post ({"limit": 4})
        assert resp.status_code == 200

        resp = cli.get ("/4?limit=4")
        assert resp.status_code == 200

        resp = cli.post ("/4", {"limit": 4})
        assert resp.status_code == 200

        resp = cli.post ("/5", {"email": "hansroh@gmail.com"})
        assert resp.status_code == 200

        resp = cli.post ("/5", {"email": "hansroh@gmail"})
        assert resp.status_code == 400

        resp = cli.post ("/5", {"uuid": "123e4567-e89b-12d3-a456-426655440000"})
        assert resp.status_code == 200

        resp = cli.post ("/5", {"uuid": "123e4567-e89b-12d3-a456-42665544000"})
        assert resp.status_code == 400

        resp = cli.post ("/5", {"uuid": "123e4567-e89b-12d3-g456-426655440000"})
        assert resp.status_code == 400

        resp = cli.post ("/6", {"a": "5"})
        assert resp.status_code == 200

        resp = cli.post ("/6", {"a": "4"})
        assert resp.status_code == 400

        resp = cli.post ("/6", {"b": "-3"})
        assert resp.status_code == 200

        resp = cli.post ("/6", {"b": "4"})
        assert resp.status_code == 400

        resp = cli.post ("/6", {"c": "1"})
        assert resp.status_code == 200

        resp = cli.post ("/6", {"c": "3"})
        assert resp.status_code == 400

        resp = cli.post ("/7", {"a": "hansroh"})
        assert resp.status_code == 200

        resp = cli.post ("/7", {"a": "xxxx"})
        assert resp.status_code == 400

        resp = cli.post ("/7", {"b": "xxxx"})
        assert resp.status_code == 200

        resp = cli.post ("/7", {"b": "xxx"})
        assert resp.status_code == 400

        resp = cli.api()("7-1").post ({"a": "xxxx"})
        assert resp.status_code == 200

        resp = cli.api()("7-1").post ({"a": "xxxxx"})
        assert resp.status_code == 400

        resp = cli.api()("7-1").post ({"a": "xxx"})
        assert resp.status_code == 400

        resp = cli.api()("7").post ({"a": "xxx"})
        assert resp.status_code == 400

        resp = cli.post ("/8", {"limit": 4})
        assert resp.status_code == 200

        resp = cli.api () ("8").post ({"limit": 4})
        assert resp.status_code == 200

        resp = cli.api () ("9").post ({"a": ''})
        assert resp.status_code == 400

        resp = cli.api () ("10").post ({"a": ''})
        assert resp.status_code == 400

        resp = cli.api () ("10").post ({"a": 'xx'})
        assert resp.status_code == 400

        resp = cli.api () ("10").post ({"a": 'yes'})
        assert resp.status_code == 200

        resp = cli.api () ("10").post ({"a": 'true'})
        assert resp.status_code == 200

        resp = cli.api () ("10").post ({"a": 'no'})
        assert resp.status_code == 200

        resp = cli.api () ("10").post ({"a": 'false'})
        assert resp.status_code == 200

        resp = cli.api () ("11").post ({"a": {"a": 1}})
        assert resp.status_code == 200

        resp = cli.api () ("11").post ({"a": [1,2]})
        assert resp.status_code == 400

        resp = cli.api () ("11").post ({"a": ''})
        assert resp.status_code == 400

        resp = cli.api () ("12").post ({"a": ''})
        assert resp.status_code == 400

        resp = cli.api () ("12").post ({"a": 0})
        assert resp.status_code == 400

        resp = cli.api () ("12").post ({"a": {}})
        assert resp.status_code == 400

        resp = cli.api () ("13").post ({"a": 1})
        assert resp.status_code == 400

        resp = cli.api () ("13").post ({"a": '1'})
        print (resp.data)
        assert resp.status_code == 200

        resp = cli.api () ("13").post ({"a": None})
        assert resp.status_code == 400

        resp = cli.api () ("13").post ({"a": None, "b": 1})
        assert resp.status_code == 400

        resp = cli.api () ("13").post ({"a": None, "b": 1.0})
        assert resp.status_code == 400

        d = dict (
            a = 'a_1',
            b = 'b_1',
            c = '1_z',
            d = '1_y',
            e = '1_x_1',
            f = '1_y_1',
        )
        resp = cli.api () ("14").post (d)
        assert resp.status_code == 200

        d1 = d.copy (); d1 ['b'] = 'a_2'
        resp = cli.api () ("14").post (d1)
        assert resp.status_code == 400

        d1 = d.copy (); d1 ['d'] = '2_z'
        resp = cli.api () ("14").post (d1)
        assert resp.status_code == 400

        d1 = d.copy (); d1 ['f'] = '2_x_2'
        resp = cli.api () ("14").post (d1)
        assert resp.status_code == 400

        d1 = d.copy (); d1 ['a'] = 'b_2'
        resp = cli.api () ("14").post (d1)
        assert resp.status_code == 400

        d1 = d.copy (); d1 ['c'] = '2_y'
        resp = cli.api () ("14").post (d1)
        assert resp.status_code == 400

        d1 = d.copy (); d1 ['e'] = '2_y_2'
        resp = cli.api () ("14").post (d1)
        assert resp.status_code == 400

        resp = cli.api () ("15").post ({"d": {"k": [5, 11]}})
        assert resp.status_code == 200

        resp = cli.api () ("15").post ({"d": {"k": [5, 9]}})
        assert resp.status_code == 400

        resp = cli.api () ("16").post ({"d": {"k": ['aa', 'aaaa']}})
        assert resp.status_code == 200

        resp = cli.api () ("16").post ({"d": {"k": ['aa', 'a']}})
        assert resp.status_code == 400

        resp = cli.api () ("16-1").post ({"d": {"k": ['aa', 'aaa']}})
        assert resp.status_code == 200

        resp = cli.api () ("16-1").post ({"d": {"k": ['aa', 'a']}})
        assert resp.status_code == 400

        resp = cli.api () ("17").post ({"d": {"k": 'aaaa'}})
        assert resp.status_code == 200

        resp = cli.api () ("17").post ({"d": {"k": 'a'}})
        assert resp.status_code == 400

        resp = cli.api () ("18").post ({"d": True})
        assert resp.status_code == 200
        assert resp.data ['r'] == 777

        resp = cli.api () ("18").post ({"d": False})
        assert resp.status_code == 444

        resp = cli.get ("19?d=777")
        assert resp.status_code == 200
        assert resp.data ['r'] == 777


def test_spec2 (app):
    @app.route ("/20", methods = ["GET", "POST"])
    def index20 (context, d, k = 1):
        return context.API (r = d)

    @app.route ("/21", methods = ["GET", "POST"])
    @app.argspec (querystrings = ["d"])
    def index21 (context, d, k = 1):
        return context.API (r = d)

    @app.route ("/22", methods = ["GET", "POST"])
    @app.argspec ()
    def index22 (context, d, k = 1):
        return context.API (r = d)

    @app.route ("/22-1", methods = ["GET", "POST"])
    @app.argspec
    def index22_1 (context, d, k = 1):
        return context.API (r = d)

    def check (context):
        assert isinstance (context.request.args ["limit"], int)

    @app.route ("/23")
    @app.argspec (querystrings = ["id", "limit"], limit = int)
    @app.depends (check)
    def index23 (context, id, limit, **DATA):
        return context.API (result = 'OK')

    @app.route ("/24", methods = ["GET", "POST"])
    @app.argspec (jsons = ['d'])
    def index24 (context, d):
        return context.API (r = d ['a'])

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("20?d=777")
        assert resp.status_code == 200

        resp = cli.post ("20?d=777", {"k": 3})
        assert resp.status_code == 400

        resp = cli.post ("22?d=777", {"k": 3})
        assert resp.status_code == 400

        resp = cli.post ("20", {"d": 777})
        assert resp.status_code == 200

        resp = cli.post ("21?k=1", {"d": 777})
        assert resp.status_code == 400

        resp = cli.post ("21", {"d": 777, "k": 2})
        assert resp.status_code == 200

        resp = cli.post ("22", {"d": 777, "k": 2})
        assert resp.status_code == 200

        resp = cli.post ("22?d=777", {"k": 2})
        assert resp.status_code == 400

        resp = cli.post ("22-1", {"d": 777, "k": 2})
        assert resp.status_code == 200

        resp = cli.post ("22-1?d=777", {"k": 2})
        assert resp.status_code == 400

        resp = cli.get ("23?id=777&limit=10")
        assert resp.status_code == 200

        resp = cli.get ("23?id=777&limit=10&x=4")
        assert resp.status_code == 400

        resp = cli.post ("23?id=777&limit=10&x=4", {})
        assert resp.status_code == 400

        resp = cli.post ("23?id=777&limit=10", dict (x = 4))
        assert resp.status_code == 200

        resp = cli.post ("23?id=777", dict (x = 4, limit = 10))
        assert resp.status_code == 400

        resp = cli.post ("24", dict (d = '{"a": 1}'))
        assert resp.status_code == 200
        assert resp.data ["r"] == 1

def test_spec3 (app):
    @app.route ("/20", methods = ["GET", "POST"])
    @app.argspec
    def index20 (context, d:str, k:int = 1):
        return context.API (r = d)

    @app.route ("/21", methods = ["GET", "POST"])
    @app.argspec (k__gt = 10)
    def index21 (context, d:str, k:int = 1):
        return context.API (r = d)

    @app.route ("/pets")
    @app.argspec (age__between = (0, 200), color__in = ["any", "black", "white"])
    def pets (context, age: int, color):
        return context.API ()

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("20")
        assert resp.status_code == 400

        resp = cli.get ("20?d=xx")
        assert resp.status_code == 200

        resp = cli.get ("20?d=xx&k=t")
        assert resp.status_code == 400

        resp = cli.get ("20?d=xx&k=2")
        assert resp.status_code == 200

        resp = cli.get ("21?d=xx&k=2")
        assert resp.status_code == 400

        resp = cli.get ("21?d=xx&k=10")
        assert resp.status_code == 400

        resp = cli.get ("21?d=xx&k=11")
        assert resp.status_code == 200

        resp = cli.get ("pets?age=10&color=black")
        assert resp.status_code == 200

        resp = cli.get ("pets?age=10&color=green")
        assert resp.status_code == 400

        resp = cli.get ("pets?age=a&color=black")
        assert resp.status_code == 400

        resp = cli.get ("pets?age=300&color=black")
        assert resp.status_code == 400


def test_spec4 (app):
    class Pet:
        age: int
        color: str = 'any'
        age__between = (0, 200)
        color__in = ["any", "black", "white"]

    @app.route ("/pets")
    @app.argspec (Pet)
    def pets (context, age, color):
        return context.API ()

    @app.route ("/pets2")
    @app.argspec (Pet)
    def pets2 (context, age:int, color = "any"):
        return context.API ()

    @app.route ("/pets3")
    @app.argspec (Pet)
    def pets3 (context, **payload):
        return context.API ()


    class Pet2:
        age: int
        def age__computed (context, val):
            if int (val) > 100:
                raise context.HttpError ("499 Bad Request")
            return int (val)

    @app.route ("/pets4")
    @app.argspec (Pet2)
    def pets4 (context, **payload):
        return context.API ()


    class Pet3:
        def age__computed (context, val):
            if int (val) > 100:
                raise context.HttpError ("499 Bad Request")
            return int (val)

    @app.route ("/pets5")
    @app.argspec (Pet3)
    def pets5 (context, **payload):
        return context.API ()


    def _testfunc (context, val):
        if int (val) > 100:
            raise context.HttpError ("499 Bad Request")
        return int (val)


    class Pet4:
        age: int
        age__computed = _testfunc

    @app.route ("/pets6")
    @app.argspec (Pet4)
    def pets6 (context, **payload):
        return context.API ()

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("pets?age=10")
        assert resp.status_code == 200

        for i in "456":
            resp = cli.get (f"pets{i}?age=10")
            assert resp.status_code == 200

            resp = cli.get (f"pets{i}?age=1000")
            assert resp.status_code == 499


def test_spec5 (app):
    os.environ ["ATILA_SET_SEPC"] = "1"
    class Pet:
        age: list
        color: str = 'any'

    @app.route ("/pets5")
    @app.argspec (Pet)
    def pets (context, **payload):
        return context.API ()

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.post ("pets5", json = {'age': [1,2], 'color': 'any'})
        assert resp.status_code == 200

    del os.environ ["ATILA_SET_SEPC"]



def test_spec6 (app):
    @app.route ("/25")
    @app.argspec
    def index25 (context, email: email):
        return context.API ()

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("25?email=asdad")
        assert resp.status_code == 400

        resp = cli.get ("25?email=asdad@asda.com")
        assert resp.status_code == 200


def test_spec7 (app):
    def func (context, val):
        return val + context.request.args ['b']

    @app.route ("/26")
    @app.argspec (ints = ['a', 'b'], b__computed = func)
    def index26 (context, a, b):
        return context.API (b = b)

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("26?a=100&b=300")
        assert resp.data == {'b': 600}
        assert resp.status_code == 200


def test_spec8 (app):
    def func (context):
        return 600

    @app.route ("/27")
    @app.argspec
    def index27 (context, a: int, b = func):
        return context.API (b = b)

    def header (name):
        def fixed (context):
            return 600
        return fixed

    @app.route ("/28")
    @app.argspec
    def index28 (context, a: int, b = header ('host')):
        return context.API (b = b)

    @app.route ("/29")
    @app.argspec
    def index29 (context, a: int, b = header ('host')):
        return context.API (b = b)

    def computed (context, val):
        return 600

    @app.route ("/31")
    @app.argspec (a__computed = computed)
    def index31 (context, a: int):
        return context.API (b = a)

    def made_600 (context):
        context.request.args ['a'] = 600

    @app.route ("/32")
    @app.argspec (computed = made_600)
    def index32 (context, a: int):
        return context.API (b = a)

    with app.test_client ("/", confutil.getroot ()) as cli:
        resp = cli.get ("27?a=100")
        assert resp.data == {'b': 600}
        assert resp.status_code == 200

        resp = cli.get ("28?a=100")
        assert resp.data == {'b': 600}
        assert resp.status_code == 200

        resp = cli.get ("29?a=100")
        assert resp.data == {'b': 600}
        assert resp.status_code == 200

        resp = cli.get ("29?a=x")
        assert resp.status_code == 400

        resp = cli.get ("31?a=1")
        assert resp.data == {'b': 600}
        assert resp.status_code == 200

        resp = cli.get ("32?a=1")
        assert resp.data == {'b': 600}
        assert resp.status_code == 200

def test_spec9 (app):
    @app.route ("/28")
    @app.argspec (optional = ['k', 'j'])
    def index28 (context, a, **payload):
        return context.API ()

    assert app._get_parameter_requirements ('index28') == \
        {'ARGS': {'k': {}, 'j': {}, 'a': {'required': True}}}


