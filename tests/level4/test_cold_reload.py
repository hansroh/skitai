import sys
import os
import time
from pathlib import Path

def test_cold_reload (launch):
    for name in ('app', 'request', 'server'):
        try:
            os.remove (f"/tmp/{name}.log")
        except FileNotFoundError:
            pass

    serve = './level4/serve-notf.py'
    with launch (serve, port = 30371, devel = True) as engine:
        Path ("./level4/my_vuejs_app/__init__.py").touch ()
        time.sleep (5)

    with open ("/tmp/server.log") as f:
        d = f.read ()
    assert d.count ("reloading app") == 1
