import atila
import atila_vue
import os
from . import services

def __config__ (pref):
    pref.config.FRONTEND = {}

def __app__ ():
    return atila.Atila (__name__)

def __setup__ (context, app):
    app.mount ('/', services)
    context.register ("myobj", "myobj")

    @context.method
    def mymethod (context):
        return context.request.uri

def __mount__ (context):
    @context.app.route ('/urlspecs')
    def urlspecs (context):
        return context.API (urlspecs = context.app.get_urlspecs ())

    @context.app.route ('/ctx')
    def ctx (context):
        return context.API (
            a = context.myobj,
            b = context.myobj2,
            c = context.mymethod (),
            d = context.mymethod2 ('hello')
        )