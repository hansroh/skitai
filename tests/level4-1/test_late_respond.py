import pytest
import os

def test_launch (launch):
    serve = '../../atila/examples/webapp/skitaid.py'
    if not os.path.isfile (serve):
        serve = '../atila/examples/webapp/skitaid.py'

    with launch (serve) as engine:
        for i in range (2):
            resp = engine.axios.get ('/apis/rest-api{}'.format (i == 1 and 2 or ''))
            assert resp.status_code == 200
            assert 'result' in resp.data
            assert 'rs4' in resp.data ['result']

        for i in range (2):
            resp = engine.axios.get ('/apis/rest-api{}'.format (i == 1 and 2 or ''))
            assert resp.status_code == 200
            assert 'result' in resp.data
            assert 'rs4' in resp.data ['result']
