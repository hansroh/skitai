import requests

def test_app_async (launch):
    with launch ("./examples/app_async.py") as engine:
        resp = engine.get ("/api5")
        assert resp.status_code == 200
        assert resp.text == 'b' * 5

        resp = engine.get ("/api4")
        assert resp.status_code == 200
        assert resp.text == 'a' * 5

        resp = engine.get ("/")
        assert resp.status_code == 200
        assert resp.text == "Hello Atila"

        resp = engine.get ("/str")
        assert resp.status_code == 200
        assert resp.text == "100"

        resp = engine.get ("/api")
        assert resp.status_code == 200
        assert resp.json ()["x"] == 100

        resp = engine.get ("/api3")
        assert resp.status_code == 200
        assert resp.json ()["x"] == 100

        resp = engine.get ("/api2")
        assert resp.status_code == 200
        assert resp.json ()["x"] == 100

