# pytest
pytest
pytest-ordering
pytest-cov
pytest-asyncio
codecov

# test deps
requests
django
psycopg2-binary
websockets
websocket_client
jinja2
aiofiles
asgiref
httpbin
wsproto
jsonrpclib-pelix
markupsafe
matplotlib
scikit-learn
pillow
opencv-python-headless<4.7.0
opencv-python<4.7.0
scikit-image
django-admin-numeric-filter
django-admin-rangefilter
aioquic<0.10; python_version>="3.6"
protobuf>=3.20
firebase-admin
python-dotenv
grpcio
lxml
lxml_html_clean
cssselect
html5lib
typing-extensions; python_version>="3.8"
