import pytest

def test_meta_info ():
    from atila.collabo.django import model
    from django_.myapp.models import MyModel, Type, Tag
    from django.db.utils import OperationalError
    from django.db.models import Model

    assert MyModel.get_table_name () == "myapp_mymodel"

    my_model = MyModel ()
    assert my_model.get_table_name () == "myapp_mymodel"
    assert my_model.get_pk () == "id"
    fks = my_model.get_fks ()
    assert set (fks.keys ()) == {'type'}
    assert fks ['type'][0] == 'type_id'

    cols = set (my_model.get_columns ())
    assert len (cols.union ({'id', 'type', 'name', 'nullable', 'blankable'})) == len (cols)

    fields = my_model.get_fields ()
    my_model.type = Type ()

    with pytest.raises (model.ValidationError) as err_inifo:
        my_model.validate_payload ({'num': 1})
    assert err_inifo.value.messages [0] == "field name is missing"

    with pytest.raises (model.ValidationError) as err_inifo:
        my_model.validate_payload (dict (type = 1))
    assert err_inifo.value.messages [0] == "field type has relation ship"

    with pytest.raises (model.ValidationError) as err_inifo:
        my_model.validate_payload (dict (name = 1))
    assert err_inifo.value.messages [0] == "field name type should be str"

    with pytest.raises (model.ValidationError) as err_inifo:
        my_model.validate_payload (dict (name = 'a'))
    assert err_inifo.value.messages [0] == "field num is missing"

    with pytest.raises (model.ValidationError) as err_inifo:
        my_model.validate_payload (dict (name = 'a', num = 'b'))
    assert err_inifo.value.messages [0] == "field num type should be int"

    my_model.validate_payload (dict (name = 'a', num = 1))
    assert my_model.num is None

    my_model.set (dict (name = 'a', num = 1))
    assert my_model.num == 1

    my_model.set (num = 2)
    assert my_model.num == 2

    assert my_model.dict () ['name'] == 'a'
    assert my_model.dict () ['num'] == 2
    assert my_model.dict () ['tags'] == []

    assert my_model.dict (['name']) ['name'] == 'a'
    assert "num" not in my_model.dict (['name'])

    with pytest.raises (model.ValidationError) as err_inifo:
        my_model.validate_payload (dict (name = 'a', num = 1, type_id = 1))
    assert err_inifo.value.messages [0] == "field type_id is not valid field"

    infos = my_model.get_field_spec ()
    print (infos)

    assert 'id' not in infos
    assert 'type' not in infos
    assert infos ['choicable']['choices'] == [('b', 'b'), ('a', 'a')]
