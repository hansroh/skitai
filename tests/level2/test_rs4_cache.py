import pytest
import time

def test_cache ():
    from rs4.misc.cache import KeyCache

    cache = KeyCache (512)
    assert cache.get (1) is None
    cache.put (1, 1)

    for i in range (600):
        cache.put (i, i)
        assert cache.get (i) == i
        time.sleep (0.01)

    assert cache.get (1) is None
    assert cache.get (84) is None
    assert cache.get (100) == 100
    assert  len (cache) == cache.maxsize


    cache = KeyCache (512, 1)
    assert cache.get (1) is None
    cache.put (1, 1)
    assert cache.get (1) == 1
    time.sleep (1.1)
    assert cache.get (1) is None

